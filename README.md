Estos sintes se usan en SuperCollider [https://supercollider.github.io/] podes usarlo desde TidalCycles [http://tidalcycles.org] pero si no queres instalar 
muchas cosas, podes mirar los tutoriales de Darien Brito (en español) https://www.youtube.com/watch?v=aSz0yN2cEyc&list=PLVApwo2lw34PHi0hH6d0Eq_SOkep67YhL
que tiene varios, incluyendo uno para controlar SuperCollider desde Ableton.

efmdos (o cualquier nombre que le quieras poner) es un sintetizador FM (frecuencia modulada) de dos operadores, uno suena (carP) el otro (modP)modula al primero.
Si usamos valores menores a uno queda una sensación de lfo (low frequency oscilator-oscilador de baja frecuencia) si usamos valores mayores la sensación de FM aumenta.

(
SynthDef(\efmdos, { |out, amp=1, attack=0.001, sustain=1, pan=0, accelerate=0, freq=440, carP=0, modP=0.8 index=8, detune=0.1|
  var env = EnvGen.ar(Env.perc(attack, 0.999, 1, -3), timeScale: sustain / 2, doneAction: 2);
  var mod = SinOsc.ar(freq * modP * Line.kr(1,1+accelerate, sustain), 0, freq * index * LFNoise1.kr(5.reciprocal).abs);
  var car = SinOsc.ar(([freq, freq+detune] * carP) + mod, 0);
  OffsetOut.ar(out, Pan2.ar(car * env, pan, amp * 0.8
	));
}).add;
)

Codigo para TidalCycles 

d1 $ n "[c3,g3,d4,fs4]" # s "efmdos" # pF "modP" "4"

primero ponemos la nota, como en cualquier sintdef, luego el nombre que le pongamos, pF funciona para llamar las variables (aquellos nombres en el primer renglon del sinte)
ATENCIÓN hay un bug o algo entre SC y Tidal que hace que los valores no puedan cambiar durante el ciclo (como si perillearamos) así que el valor que le pongamos
en el pF va a variar de ciclo en ciclo, razon por la cual pF "modP" (slow 2 $ range 2 4 tri) no va a funcionar igual que en otro sinte o en un sample, de todos
modos las posibilidades son muchas.

efmtres es igual que el sinte anteriór, sumandole un segundo modulador (moduP) y, quizá lo mas interesante, una envolvente adsr en los moduladores (modP y moduP)
esto permite nuevos efectos interesantes, se llama de la misma manera que el sinte anteriór.

(
SynthDef(\efmtres, { |out, amp=1, att=0, dec=0, sus=1,  rel=2, pan=0, accelerate=0, freq=440, carP=1, modP=3, moduP =2, index=3, mul=0.1, detune=0, at=0.5, de=0, su=2, re=0|
	var env = EnvGen.ar(Env.adsr(att, dec, sus, rel));
	var mod = SinOsc.ar(freq * modP * EnvGen.kr(Env.adsr(at, de,su,re,2,-4,0)), 0, freq * index * LFNoise1.kr(5.reciprocal).abs);
	var modu = SinOsc.ar(freq * moduP * EnvGen.kr(Env.adsr(at, de,su,re,2,-4,0)), 0, freq * index * LFNoise1.kr(5.reciprocal).abs);
	var car = SinOsc.ar(([freq, freq+detune] * carP) + [mod + modu], 0, mul);
  OffsetOut.ar(out, Pan2.ar(car * env, pan, amp * 2));
}).add;
)



A tener en cuenta:
index es una suerte de ammount (cantidad) de efecto que causa el modP (o moduP en efmtres) en el carP, hasta donde se podemos poner valores muy altos de
index (1000 o mas) eso va a hacer que el sonido se destruya mucho, aspecto a veces buscado. 

Sintes en uso (set Sala Puerta X)

https://gitlab.com/snippets/1884923